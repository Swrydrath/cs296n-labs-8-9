﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CorvallisOR;
using CorvallisOR.Controllers;
using CorvallisOR.DAL;
using CorvallisOR.Models;

namespace CorvallisORTests
{
    [TestClass]
    public class UnitTests
    {
        private List<Member> memberList = new List<Member> {
            new Member { Id="1", Name="Sean White", Email="sWhite@test.net", UserName="sWhite" },
            new Member { Id="2", Name="Brian Bird", Email="bBird@test.net", UserName="bBird" },
            new Member { Id="3", Name="Merrick Simms", Email="mSimms@test.com", UserName="mSimms" }
        };
        private List<Topic> topicList = new List<Topic> {
            new Topic { TopicID=1, Subject="Topic One" },
            new Topic { TopicID=2, Subject="Topic Two" }
        };
        private List<Message> messages;

        [TestMethod]
        public void TestIndex()
        {
            messages = new List<Message> {
            new Message { MessageID=1, Date=Convert.ToDateTime("1/2/3"), Body="Test Message One",
                Member=memberList[0], Topic=topicList[1] },
            new Message { MessageID=2, Date=Convert.ToDateTime("3/2/1"), Body="Test Message Two",
                Member=memberList[1], Topic=topicList[0] },
            new Message { MessageID=3, Date=Convert.ToDateTime("7/7/77777"), Body="Test Message Three",
                Member=memberList[2], Topic=topicList[1] }
        };

            var repo = new MockRepo(messages);
            var messenger = new ForumController(repo);
            var result = (ViewResult)messenger.Index();
            var model = (List<Message>)result.Model;
            Assert.AreEqual(model.Count, 3);
            Assert.AreEqual(model[0].Member.Name, "Sean White");
            Assert.AreEqual(model[2].Topic.Subject, "Topic Two");
        }

        [TestMethod]
        public void TestSearch()
        {
            messages = new List<Message> {
            new Message { MessageID=1, Date=Convert.ToDateTime("7/8/9"), Body="Test Message One",
                Member=memberList[0], Topic=topicList[1] },
            new Message { MessageID=2, Date=Convert.ToDateTime("9/8/7"), Body="Test Message Two",
                Member=memberList[1], Topic=topicList[0] },
            new Message { MessageID=3, Date=Convert.ToDateTime("7/7/77777"), Body="Test Message Three",
                Member=memberList[2], Topic=topicList[1] }
        };
            var repo = new MockRepo(messages);
            var messenger = new ForumController(repo);
            var result = (ViewResult)messenger.Search("Two");
            var model = (List<Message>)result.Model;
            Assert.AreEqual(model.Count, 2);
            Assert.AreEqual(model[0].MessageID, 1);
            Assert.AreEqual(model[1].MessageID, 3);
        }
    }
}
