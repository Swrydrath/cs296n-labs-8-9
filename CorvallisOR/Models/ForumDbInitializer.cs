﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CorvallisOR.Models
{
    public class ForumDbInitializer : System.Data.Entity.DropCreateDatabaseAlways<ForumDb>
    {
        protected UserManager<Member> userManager = new UserManager<Member>(
                new UserStore<Member>(new ForumDb()));

        protected override void Seed(ForumDb context)
        {
            base.Seed(context);          
        }
    }   
}