﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CorvallisOR.Models
{
    public class Member : IdentityUser
    {
        public string Name { get; set; }
    }
}