﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CorvallisOR.Models
{
    public class CreateMessageVM
    {
        public int ID { get; set; }
        public int? TopicID { get; set; }
        [Required(ErrorMessage = "A subject is required")]
        public string Subject { get; set; }
        public IEnumerable<SelectListItem> Topics { get; set; }
        public Message Message { get; set; }
    }
}