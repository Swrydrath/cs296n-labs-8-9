﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;


namespace CorvallisOR.Models
{
    public class News
    {
        public virtual DateTime Date { get; set; }
        public string Title { get; set; }
        public string Story { get; set; }
    }
}