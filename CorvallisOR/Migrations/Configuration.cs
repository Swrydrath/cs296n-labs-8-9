﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CorvallisOR.Models;

namespace CorvallisOR.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<CorvallisOR.Models.ForumDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(CorvallisOR.Models.ForumDb context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Sean White" },
            //      new Person { FullName = "Brian Bird" },
            //      new Person { FullName = "Merrick Simms" }
            //    );
            //
            //if (System.Diagnostics.Debugger.IsAttached == false) System.Diagnostics.Debugger.Launch();
            context.Roles.AddOrUpdate(new IdentityRole() { Name = "SuperAdmin" });
            context.Roles.AddOrUpdate(new IdentityRole() { Name = "Admin" });
            context.Roles.AddOrUpdate(new IdentityRole() { Name = "Standard" });
            Member member = new Member { Name = "Sean White", Email = "sWhite@test.com", UserName = "sWhite" };
            var store = new UserStore<Member>(context);
            var userManager = new UserManager<Member>(store);
            var result = userManager.Create(member, "Password1");
            userManager.AddToRole(member.Id, "SuperAdmin");
            userManager.AddToRole(member.Id, "Admin");
            userManager.AddToRole(member.Id, "Standard");
            member = new Member { Name = "Brian Bird", Email = "bBird@test.com", UserName = "bBird" };
            result = userManager.Create(member, "Password2");
            userManager.AddToRole(member.Id, "Admin");
            userManager.AddToRole(member.Id, "Standard");
            member = new Member { Name = "Merrick Simms", Email = "mSimms@test.com", UserName = "mSimms" };
            result = userManager.Create(member, "Password3");
            userManager.AddToRole(member.Id, "Standard");
            context.Topics.AddOrUpdate(new Topic { Subject = "Parents' Night Out" });
            context.Topics.AddOrUpdate(new Topic { Subject = "Family Movie Swim" });
            context.Topics.AddOrUpdate(new Topic { Subject = "Kayak Roll Session" });
            context.SaveChanges();
            context.Messages.Add(new Message
            {
                Topic = context.Topics.First(),
                Date = DateTime.Now,
                Member = context.Users.First(),
                Body = "Parents' Night Out Drop off the kids while you go out for a night on the town."
            });
            context.Messages.Add(new Message
            {
                Topic = context.Topics.SingleOrDefault(t => t.Subject == "Family Movie Swim"),
                Date = DateTime.Now,
                Member = context.Users.SingleOrDefault(u => u.UserName == "sWhite"),
                Body = "Family Movie Swim A family-friendly movie will be projected pool side each month."
            });
            context.Messages.Add(new Message
            {
                Topic = context.Topics.SingleOrDefault(t => t.Subject == "Kayak Roll Session"),
                Date = DateTime.Now,
                Member = context.Users.SingleOrDefault(u => u.UserName == "bBird"),
                Body = "Kayak Roll Session: Bring in your kayak and paddle around the indoor 50 meter pool."
            });
            context.Messages.Add(new Message
            {
                Topic = context.Topics.First(),
                Date = DateTime.Parse("7/7/77"),
                Member = context.Users.SingleOrDefault(u => u.UserName == "sWhite"),
                Body = "Kids ages 3 to 6 will play games in the activity room and those 7 to 12 can also go for a swim!"
            });
        }
    }
}
