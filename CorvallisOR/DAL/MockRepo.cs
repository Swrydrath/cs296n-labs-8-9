﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CorvallisOR.Models;

namespace CorvallisOR.DAL
{
    public class MockRepo : IRepoDB
    {
        private List<Message> messages;
        private List<Topic> topics = new List<Topic>();
        private List<Member> members = new List<Member>();

        public MockRepo()
        {
            messages = new List<Message>();
        }
        public MockRepo(List<Message> messenger)
        {
            messages = messenger;
        }

        public void AddMessage(Message message)
        {
            messages.Add(message);
        }

        public void AddTopic(Topic topic)
        {
            topics.Add(topic);
        }

        public bool CheckSingleSubject(Topic oldTopic)
        {
            return messages.Where(m => m.Topic.Subject == oldTopic.Subject).Count() == 1;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Member FindMemberById(string id)
        {
            return members.FirstOrDefault(x => x.Id == id);
        }

        public Message FindMessageById(int? id)
        {
            return messages.SingleOrDefault(m => m.MessageID == id);
        }

        public Topic FindTopicById(int? id)
        {
            return topics.SingleOrDefault(t => t.TopicID == id);
        }

        public Topic FindTopicBySubject(string subject)
        {
            return topics.SingleOrDefault(t => t.Subject == subject);
        }

        public List<Message> GetMessageList()
        {
            return messages;
        }

        public System.Web.Mvc.SelectList GetTopicSelectList()
        {
            return new SelectList(topics.OrderBy(t => t.Subject), "TopicID", "Subject");
        }

        public void MessageModified(Message message)
        {
            return;
        }

        public void RemoveMessage(Message message)
        {
            messages.Remove(message);
        }

        public void RemoveTopic(Topic topic)
        {
            topics.Remove(topic);
        }

        public void SaveChanges()
        {
            return;
        }

        public List<Message> SearchMessages(string searchString)
        {
            return messages.Where(m => m.Topic.Subject.Contains(searchString)).ToList();
        }

        public void TopicModified(Topic oldTopic)
        {
            return;
        }
    }
}