﻿using System;
using CorvallisOR.Models;
namespace CorvallisOR.DAL
{
    public interface IRepoDB
    {
        void AddMessage(Message message);
        void AddTopic(Topic topic);
        bool CheckSingleSubject(Topic oldTopic);
        void Dispose();
        Member FindMemberById(string id);
        Message FindMessageById(int? id);
        Topic FindTopicById(int? id);
        Topic FindTopicBySubject(string subject);
        System.Collections.Generic.List<Message> GetMessageList();
        System.Web.Mvc.SelectList GetTopicSelectList();
        void MessageModified(Message message);
        void RemoveMessage(Message message);
        void RemoveTopic(Topic topic);
        void SaveChanges();
        System.Collections.Generic.List<Message> SearchMessages(string searchString);
        void TopicModified(Topic oldTopic);
    }
}
