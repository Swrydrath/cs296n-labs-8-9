﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CorvallisOR.Models;

namespace CorvallisOR.DAL
{
    public class RepoDB : IDisposable, IRepoDB
    {
        private ForumDb db = new ForumDb();

        public List<Message> GetMessageList()
        {
            return db.Messages.ToList();
        }

        public Message FindMessageById(int? id)
        {
            return db.Messages.Find(id);
        }

        public SelectList GetTopicSelectList()
        {
            return new SelectList(db.Topics.OrderBy(t => t.Subject), "TopicID", "Subject");
        }

        public Topic FindTopicById(int? id)
        {
            return db.Topics.Find(id);
        }

        public void AddTopic(Topic topic)
        {
            db.Topics.Add(topic);
            db.SaveChanges();
        }

        public Member FindMemberById(string id)
        {
            return db.Users.FirstOrDefault(x => x.Id == id);
        }

        public void AddMessage(Message message)
        {
            db.Messages.Add(message);
            db.SaveChanges();
        }

        public bool CheckSingleSubject(Topic oldTopic)
        {
            return db.Messages.Where(m => m.Topic.Subject == oldTopic.Subject).Count() == 1;
        }

        public Topic FindTopicBySubject(string subject)
        {
            return db.Topics.SingleOrDefault(t => t.Subject == subject);
        }

        public void TopicModified(Topic oldTopic)
        {
            db.Entry(oldTopic).State = EntityState.Modified;
        }

        public void MessageModified(Message message)
        {
            db.Entry(message).State = EntityState.Modified;
        }

        public void SaveChanges()
        {
            db.SaveChanges();
        }

        public void RemoveTopic(Topic topic)
        {
            db.Topics.Remove(topic);
        }

        public void RemoveMessage(Message message)
        {
            db.Messages.Remove(message);
        }
        public List<Message> SearchMessages(string searchString)
        {
            return db.Messages.Where(m => m.Topic.Subject.Contains(searchString)).ToList();
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}