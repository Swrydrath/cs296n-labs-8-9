﻿using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CorvallisOR.Models;
using CorvallisOR.DAL;

namespace CorvallisOR.Controllers
{
    public class ForumController : Controller
    {
        private IRepoDB db;

        public ForumController()
        {
            db = new RepoDB();
        }

        public ForumController(IRepoDB repo)
        {
            db = repo;
        }

        // GET: Forum
        public ActionResult Index()
        {
            return View(db.GetMessageList());
        }

        // GET: Forum/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.FindMessageById(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return View(message);
        }

        // GET: Forum/Create
        [Authorize(Roles = "Standard")]
        public ActionResult Create()
        {
            CreateMessageVM createVM = new CreateMessageVM
            {
                Topics = db.GetTopicSelectList(),
                Message = new Message()
            };
            return View(createVM);
        }

        // POST: Forum/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Standard")]
        //[ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TopicID,Topics,Message,Subject")]CreateMessageVM createVM)
        {
            if (ModelState.IsValid)
            {
                Message message = createVM.Message;
                message.Topic = db.FindTopicById(createVM.TopicID);
                if (message.Topic == null || message.Topic.Subject != createVM.Subject)
                {
                    Topic topic = new Topic { Subject = createVM.Subject };
                    db.AddTopic(topic);
                    message.Topic = topic;
                }
                string currentUserId = User.Identity.GetUserId();
                message.Member = db.FindMemberById(currentUserId);
                message.Date = DateTime.Now;
                db.AddMessage(message);
                return RedirectToAction("Index");
            }
            else
            {
                createVM.Topics = db.GetTopicSelectList();
                createVM.Message = new Message();
            }

            return View(createVM);
        }

        // GET: Forum/Edit/5
        [Authorize(Roles = "Standard")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.FindMessageById(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            if (message.Member.Id != User.Identity.GetUserId())
            {
                return new HttpStatusCodeResult((int)HttpStatusCode.Forbidden);
            }
            CreateMessageVM createVM = new CreateMessageVM
            {
                ID = message.MessageID,
                Topics = db.GetTopicSelectList(),
                Message = message
            };
            createVM.Subject = message.Topic.Subject;
            return View(createVM);
        }

        // POST: Forum/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Standard")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,TopicID,Topics,Message,Subject")]CreateMessageVM createVM)
        {
            bool isDirty = false;
            bool updateTopic = false;
            Topic topic = new Topic();
            if (ModelState.IsValid)
            {
                Message message = db.FindMessageById(createVM.ID);
                Topic oldTopic = db.FindTopicById(message.Topic.TopicID);
                if (oldTopic.Subject != createVM.Subject)
                {
                    isDirty = true;
                    if (db.CheckSingleSubject(oldTopic)) updateTopic = true;
                    topic = db.FindTopicById(createVM.TopicID);
                    if (topic == null)
                    {
                        topic = db.FindTopicBySubject(createVM.Subject);
                        if (topic == null)
                        {
                            topic = new Topic { Subject = createVM.Subject };
                        }
                    }
                }
                if (message.Body != createVM.Message.Body)
                {
                    isDirty = true;
                    message.Body = createVM.Message.Body;
                }
                if (isDirty)
                {
                    if (updateTopic)
                    {
                        oldTopic.Subject = topic.Subject;
                        db.TopicModified(oldTopic);
                        topic = oldTopic;
                    }
                    message.Topic = topic;
                    db.MessageModified(message);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            return View(createVM);
        }

        // GET: Forum/Delete/5
        [Authorize(Roles = "Standard")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = db.FindMessageById(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            if (message.Member.Id != User.Identity.GetUserId())
            {
                return new HttpStatusCodeResult((int)HttpStatusCode.Forbidden);
            }
            return View(message);
        }

        // POST: Forum/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Standard")]
        public ActionResult DeleteConfirmed(int id)
        {
            Message message = db.FindMessageById(id);
            Topic topic = db.FindTopicById(message.Topic.TopicID);
            db.RemoveMessage(message);
            if (db.CheckSingleSubject(message.Topic))
            {
                db.RemoveTopic(topic);
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Search(string searchString)
        {
            ViewBag.SearchString = searchString;
            return View(db.SearchMessages(searchString));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
